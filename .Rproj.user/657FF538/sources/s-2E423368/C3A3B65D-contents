---
title: "Dealing with spatial with R <br> Short intro"
output: html_notebook
---

# Get prepared

## Download the data :

- Spatial layer of [Austria](https://www.data.gv.at/katalog/dataset/81f390f9-9b42-3a72-ad9b-e11a4dbebab0)
- [Census data](https://www.data.gv.at/katalog/dataset/3bfba412-7053-3a60-937a-8c3dd2c71294)
- More [spatial layers](https://mapcruzin.com/free-austria-arcgis-maps-shapefiles.htm)

## Install the packages :

```{r,eval=FALSE}
install.packages(c("tidyverse","sf","cartography","leaflet"))
```

## Load the packages

```{r,message=FALSE,warning=FALSE}
require(tidyverse)
require(sf)
require(cartography)
require(leaflet)
```

## Import spatial data, inspect and plot

```{r}
map <- st_read("Geo/STATISTIK_AUSTRIA_POLBEZ_20180101.shp")
str(map)
st_geometry(map) %>% plot()
```


# My first map

Create random variables (one integer, one float) to draw in the map

```{r}
dat <- map %>% mutate(int = rpois(nrow(map),lambda = 10000),float=rnorm(nrow(map),mean = 10,sd = 2))
choroLayer(dat,var = "float")
```


## Play around with options

```{r}
choroLayer(dat,var = "float",legend.pos = "topleft",nclass = 5,col=blues9[1:5])
```

## Add a proportional symbol layer


```{r}
choroLayer(dat,var = "float",legend.pos = "topleft",nclass = 5,col=blues9[1:5])
propSymbolsLayer(dat,var = "int")
```

```{r}
choroLayer(dat,var = "float",legend.pos = "topleft",nclass = 5,col=blues9[1:5])
propSymbolsLayer(dat,var = "int",inches = .1)
```

## Both at once 

```{r}
st_geometry(dat) %>% plot()
propSymbolsChoroLayer(dat,var="int",var2="float",inches = .1,legend.var2.pos = "topleft",legend.var.pos = "bottomleft")
```


# Multiple layers

## Reprojections

Sometimes, coordinates systems differ... And we cannot stack layers !

```{r}
rail <- st_read("Geo/railways.shp")
st_crs(rail);st_crs(dat)
```

```{r}
plot(st_geometry(dat))
plot(st_geometry(rail),color="blue",add=T)
```

```{r}
reproj_dat <- st_transform(dat,crs = "+proj=longlat +datum=WGS84")
reproj_rail <- st_transform(rail,crs = "+proj=longlat +datum=WGS84")
plot(st_geometry(reproj_dat))
plot(st_geometry(reproj_rail),col="blue",add=T)
```


# Go interactive !

First, we have to reproject the map to fit the mercator coordinates

```{r}
```


```{r}
leaflet() %>% 
  addPolygons(data=reproj) %>% 
  addTiles()
```

Resources : 
- Cartography package
- More on geocomputation :
    + [The sf package](https://github.com/r-spatial/sf)
    + [Manual for geocomputations](https://geocompr.robinlovelace.net/)
- [](Leaflet package)